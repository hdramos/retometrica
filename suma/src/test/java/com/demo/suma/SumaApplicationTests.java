package com.demo.suma;

import com.demo.suma.entidades.Suma;
import com.demo.suma.repositorio.SumaRepositorio;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
@SpringBootTest
public class SumaApplicationTests {
	@Autowired
	private SumaRepositorio sumaRepositorio;

	@Test
	public void GuardarSuma() {
		Suma suma = new Suma();
		suma.setSumando01(10);
		suma.setSumando02(20);
		suma.setResultado(30);
		Suma c = sumaRepositorio.save(suma);
		Assert.assertNotNull(c);
	}
}
package com.demo.suma.serviciosrest;

import com.demo.suma.entidades.Suma;
import com.demo.suma.negocio.Negocio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class SumaRest {
    @Autowired
    private Negocio negocio;

    @GetMapping("/suma/{num1}/{num2}")
    public Suma obtenerResultado(@PathVariable(value = "num1") int num1, @PathVariable(value = "num2") int num2){
        int rpta = num1 + num2;

        Suma suma = new Suma();
        suma.setSumando01(num1);
        suma.setSumando02(num2);
        suma.setResultado(rpta);

        negocio.realizarSuma(suma);

        return suma;
    }
}
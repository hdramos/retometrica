package com.demo.suma.entidades;

import javax.persistence.*;

@Entity
@Table(name = "suma")
public class Suma {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "codigo")
    private Integer codigo;
    private Integer sumando01;
    private Integer sumando02;
    private Integer resultado;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getSumando01() {
        return sumando01;
    }

    public void setSumando01(Integer sumando01) {
        this.sumando01 = sumando01;
    }

    public Integer getSumando02() {
        return sumando02;
    }

    public void setSumando02(Integer sumando02) {
        this.sumando02 = sumando02;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }
}

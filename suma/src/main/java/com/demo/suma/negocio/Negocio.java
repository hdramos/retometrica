package com.demo.suma.negocio;

import com.demo.suma.entidades.Suma;
import com.demo.suma.repositorio.SumaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Negocio {
    @Autowired
    private SumaRepositorio sumaRepositorio;

    public Suma realizarSuma(Suma suma){
        return sumaRepositorio.save(suma);
    }
}
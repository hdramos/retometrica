package com.demo.suma.repositorio;

import com.demo.suma.entidades.Suma;
import org.springframework.data.repository.CrudRepository;

public interface SumaRepositorio extends CrudRepository<Suma,Integer> {
}